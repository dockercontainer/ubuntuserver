# This Dockerfile creates an Ubuntu 20.04 image

# Pull base image to build buildroot
FROM ubuntu:20.04 

LABEL maintainer="jensamberg@mail.de"

# Setup environment
ENV DEBIAN_FRONTEND noninteractive
ENV TZ=Europe/Berlin

RUN dpkg --add-architecture i386 &&\
    apt-get update -y
# Install.
RUN \
  apt-get update && \
  apt-get -y upgrade && \
  apt-get install -y tzdata && \
  apt-get install -y build-essential sed mtools libfreetype6 libfontconfig1   && \
  apt-get install -y curl git htop man unzip vim wget cmake subversion gcovr && \
  apt-get install -y mercurial make binutils gcc g++ patch bc file rsync zip cpio tar && \
  apt-get install -y gcc-multilib g++-multilib libncurses5-dev valgrind && \
  apt-get install -y libgl-dev sqlite sshpass && \
  apt-get install -y locales && \
  apt-get -y autoremove && \
  apt-get -y clean 

RUN locale-gen en_US.UTF-8

ENV LC_ALL en_US.UTF-8





    



